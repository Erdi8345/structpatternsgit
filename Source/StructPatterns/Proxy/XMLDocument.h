// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * Simple lass to represent same third-party XML library
 */
class STRUCTPATTERNS_API XMLDocument final 
{
public:
	static XMLDocument ParseDoc(const char* Name) { return {}; };

	bool IsValid() const { return true; };

	int ReadInt(const char* Name) { return 0; };

	double ReadDouble(const char* Name) { return 0.f; };

	FString ReadString(const char* Name) { return ""; };

	void WriteInt(const char* Name, int Value){};

	void WriteDouble(const char* Name, double Value){};

	void WriteString(const char* Name, const char* Value){};
};
