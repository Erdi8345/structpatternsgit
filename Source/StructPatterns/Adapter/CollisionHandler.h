// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "DamageHandler.h"
#include "Weapon.h"
#include "Components/ActorComponent.h"
#include "CollisionHandler.generated.h"

class AWeapon;
class UDamageHandler;

UCLASS(meta=(BlueprintSpawnableComponent) )
class STRUCTPATTERNS_API UCollisionHandler : public UActorComponent
{
	GENERATED_BODY()

protected:
	/** @return Damage handling component. */
	UDamageHandler* GetDamageHandler();

	/** Override it since all the components are already inited at this point. */
	void BeginPlay() override;

	/** Bind to the UPrimitiveComponent overlap event. */
	void BindComponent();

	/** Handle incoming overlap event. */
	UFUNCTION()
		void OnActorOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	/** Choose proper function call based on the component tag. */
	void SwitchByComponentTag(UPrimitiveComponent* Comp, AWeapon* Weapon);

private:
	/** Save reference to this component for quick access later. */
	UPROPERTY()
		UDamageHandler* DamageHandler;
		
};

inline void UCollisionHandler::BeginPlay()
{
	Super::BeginPlay();
	BindComponent();
}

inline UDamageHandler* UCollisionHandler::GetDamageHandler()
{
	if(!DamageHandler)
	{
		DamageHandler = Cast<UDamageHandler>(GetOwner()->GetComponentByClass(UDamageHandler::StaticClass()));
	}

	return DamageHandler;
}

inline void UCollisionHandler::BindComponent()
{
	if(GetOwner())
	{
		TArray<UPrimitiveComponent*> Components;
		GetOwner()->GetComponents(Components);
		UPrimitiveComponent* Collider = nullptr;
		for(auto Component : Components)
		{
			Component->OnComponentBeginOverlap.AddDynamic(this, &UCollisionHandler::OnActorOverlapBegin);
		}
	}
}

inline void UCollisionHandler::OnActorOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(OverlappedComp && OtherActor->GetClass()->IsChildOf(AWeapon::StaticClass()))
	{
		SwitchByComponentTag(OverlappedComp, Cast<AWeapon>(OtherActor));
	}
}

inline void UCollisionHandler::SwitchByComponentTag(UPrimitiveComponent* Comp, AWeapon* Weapon)
{
	if(GetDamageHandler())
	{
		/** No need to expose these statics to the global scope since we use them in here only. */
		static const FName Head = "Head";
		static const FName Body = "Body";
		static const FName Arms = "Arms";
		static const FName Legs = "Legs";

		if(Comp->ComponentHasTag(Head))
		{
			GetDamageHandler()->DamageHead(Weapon);
		}
		else if(Comp->ComponentHasTag(Body))
		{
			GetDamageHandler()->DamageBody(Weapon);
		}
		else if (Comp->ComponentHasTag(Arms))
		{
			GetDamageHandler()->DamageBody(Weapon);
		}
		else if (Comp->ComponentHasTag(Legs))
		{
			GetDamageHandler()->DamageBody(Weapon);
		}
	}
}
