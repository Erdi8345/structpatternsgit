// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DamageHandler.generated.h"

class AWeapon;

UCLASS(meta=(BlueprintSpawnableComponent) )
class STRUCTPATTERNS_API UDamageHandler : public UActorComponent
{
	GENERATED_BODY()

public:
	/** Calculate head damage by weapon. */
	void DamageHead(AWeapon* Weapon){};

	/** Calculate body damage by weapon */
	void DamageBody(AWeapon* Weapon){};

	/** Calculate arms damage by weapon */
	void DamageArms(AWeapon* Weapon){};

	/** Calculate legs damage by weapon */
	void DamageLegs(AWeapon* Weapon){};

};
