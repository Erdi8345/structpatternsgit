// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "IKHandlingComponent.h"
#include "InputHandlingComponent.h"
#include "Components/ActorComponent.h"
#include "InteractionFacade.generated.h"

class USkeletalMeshSocket;
class UIKHandlingComponent;
class UInputHandlingComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class STRUCTPATTERNS_API UInteractionFacade : public UActorComponent
{
	GENERATED_BODY()

		using InputHandler = UInputHandlingComponent;
		using IKHandler = UIKHandlingComponent;
		using Socket = USkeletalMeshSocket;

public:
	UFUNCTION(BlueprintCallable, Category = "Interaction")
		virtual USkeletalMeshSocket* GetInteractionSocket() const;

	/** Begin interaction between the owner and the given target */
	UFUNCTION(BlueprintCallable, Category = "Interaction")
		virtual void InteractionBegin(AActor* Target);

	/** End interaction between the owner and the given target */
	UFUNCTION(BlueprintCallable, Category = "Interaction")
		virtual void InteractionEnd(AActor* Target);

	
};

inline USkeletalMeshSocket* UInteractionFacade::GetInteractionSocket() const
{
	Socket* Result = nullptr;

	auto IK = Cast<IKHandler>(GetOwner()->GetComponentByClass(IKHandler::StaticClass()));

	if(IK)
	{
		Result = IK->GetInteractionSocket();
	}

	return Result;
}

inline void UInteractionFacade::InteractionBegin(AActor* Target)
{
	if(auto Input = Cast<InputHandler>(GetOwner()->GetComponentByClass(InputHandler::StaticClass())))
	{
		Input->LockPlayerInput();
	}

	auto IK = Cast<IKHandler>(GetOwner()->GetComponentByClass(IKHandler::StaticClass()));
	const auto TargetIK = Cast<IKHandler>(Target->GetComponentByClass(IKHandler::StaticClass()));
	if(IK && TargetIK)
	{
		IK->UseSocketTransformBegin(TargetIK->GetInteractionSocket());
	}
}

inline void UInteractionFacade::InteractionEnd(AActor* Target)
{
	if (auto Input = Cast<InputHandler>(GetOwner()->GetComponentByClass(InputHandler::StaticClass())))
	{
		Input->UnlockPlayerInput();
	}

	auto IK = Cast<IKHandler>(GetOwner()->GetComponentByClass(IKHandler::StaticClass()));
	const auto TargetIK = Cast<IKHandler>(Target->GetComponentByClass(IKHandler::StaticClass()));
	if (IK && TargetIK)
	{
		IK->UseSocketTransformEnd(TargetIK->GetInteractionSocket());
	}
}

